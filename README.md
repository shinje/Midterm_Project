# Software Studio 2018 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer

## Topic
* ChatTW
* Key functions (add/delete)
    1. 電子信箱註冊並登入
    2. Google登入
    3. 搜尋其他用戶
    4. 加為好友
    5. 開始聊天（對話紀錄皆會保存）
* Other functions (add/delete)
    1. 註冊時會收到驗證信，避免亂用別人信箱註冊
    1. 更改頭貼、名稱、手機號碼、密碼
    2. 搜尋用戶時，信箱、名稱、手機皆可使用
    3. 將對方加為好友，對方若拒絕，申請的人不會知道（只會看到交友已送出），避免被騷擾也避免尷尬
    4. 拒絕對方好友邀請的人，日後可以再將對方加為好友
    5. 聊天時，有新訊息將會自動滾動到底

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|20%|Y|
|GitLab Page|5%|Y| (Host on firebase)
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|15%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|N|
|Use CSS Animation|2.5%|N|
|Security Report|5%|N|
|Other functions|1~10%|N|

## Website Detail Description

## Security Report (Optional)
