const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');

const srcPath = path.resolve(__dirname, 'src');
const distPath = path.resolve(__dirname, 'dist');

module.exports = {
    context: srcPath,
    resolve: {
        alias: {
            images: path.resolve(srcPath, 'images'),
            components: path.resolve(srcPath, 'components')
        }
    },
    entry: {
        index: [
            '@fortawesome/fontawesome',
            '@fortawesome/fontawesome-free-solid',
            'bootstrap',
            'bootstrap/dist/css/bootstrap.min.css',
            'firebase/auth',
            'firebase/firestore',
            'firebase/storage',
            'babel-polyfill',
            './index.jsx'
        ]
    },
    output: {
        path: distPath,
        filename: '[name].bundle.js',
        chunkFilename: '[name].bundle.js'
    },
    module: {
        rules: [{
            test: /\.jsx$/,
            exclude: [/node_modules/],
            use: {
                loader: 'babel-loader',
                options: {
                    presets: ['env', 'react'],
                    plugins: [
                        'transform-object-rest-spread',
                        'transform-class-properties',
                        'syntax-dynamic-import'
                    ],
                    cacheDirectory: true
                }
            }
        }, {
            test: /\.css$/,
            use: ['style-loader',
                {
                    loader: 'css-loader',
                    options: {
                        url: false
                    }
                }
            ],
        }, {
            test: /\.(jpe?g|png|gif|svg)$/,
            use: [{
                loader: 'url-loader',
                options: {
                    limit: 40000
                }
            }, 'image-webpack-loader']
        }]
    },
    optimization: {
        splitChunks: {
            chunks: "all"
        }
    },
    plugins: [
        new CleanWebpackPlugin(['dist']),
        new HtmlWebpackPlugin({
            title: 'ChatTW',
            favicon: 'images/favicon.png',
            template: 'index.html',
            hash: true
        }),
        new webpack.ProvidePlugin({
            React: 'react',
            ReactDOM: 'react-dom',
            firebase: 'firebase/app',
            ButtonGroup: ['reactstrap', 'ButtonGroup'],
            Button: ['reactstrap', 'Button'],
            FormGroup: ['reactstrap', 'FormGroup'],
            FormFeedback: ['reactstrap', 'FormFeedback'],
            FormText: ['reactstrap', 'FormText'],
            Label: ['reactstrap', 'Label'],
            Col: ['reactstrap', 'Col'],
            Input: ['reactstrap', 'Input'],
            InputGroup: ['reactstrap', 'InputGroup'],
            InputGroupAddon: ['reactstrap', 'InputGroupAddon'],
            Alert: ['reactstrap', 'Alert'],
            profile: 'images/profile.jpg'
        })
    ],
    devtool: 'source-map',
    mode: 'development'
};