import Main from 'components/Main.jsx';

import './index.css';

window.onload = () => {
    const config = {
        apiKey: "AIzaSyAr0JffAcTKwusIh55VafwwXPJuzBDmSW4",
        authDomain: "chat-tw.firebaseapp.com",
        databaseURL: "https://chat-tw.firebaseio.com",
        projectId: "chat-tw",
        storageBucket: "chat-tw.appspot.com",
        messagingSenderId: "531477373158"
    };
    firebase.initializeApp(config);
    ReactDOM.render(
        <Main />,
        document.getElementById('root')
    );
};