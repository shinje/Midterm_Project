import Header from 'components/Header.jsx';
import Login from 'components/Login.jsx';
import Pages from 'components/Pages.jsx';

import './Main.css';

export default class Main extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            user: null,
            ready: false,
            showRoom: false
        };
    }

    render() {
        return (
            <div className='main container text-light'>
                {
                    !this.state.showRoom && <Header show={!this.state.showRoom} />
                }
                {
                    this.state.ready &&
                    (this.state.user ?
                        <Pages user={this.state.user} showRoom={this.state.showRoom}
                            onShowRoom={this.handleShowRoom} onHideRoom={this.handleHideRoom} /> :
                        <Login onSignInSuccess={this.handleSignInSuccess} />)
                }
            </div>
        );
    }

    componentDidMount() {
        firebase.auth().onAuthStateChanged(user => {
            this.setState({
                user: user && user.emailVerified ? user : null,
                ready: true
            });
        });
    }

    componentWillUnmount() {
        firebase.auth().onAuthStateChanged(() => { });
    }

    handleSignInSuccess = () => {
        firebase.firestore().doc(`users/${this.state.user.uid}`).get().then(doc => {
            if (!doc.exists) {
                doc.ref.set({
                    email: this.state.user.email,
                    displayName: this.state.user.email.replace(/@.+/, ''),
                    photoURL: this.state.user.photoURL ? this.state.user.photoURL : profile,
                    phoneNumber: this.state.user.phoneNumber ? this.state.user.phoneNumber : '',
                });
            }
        });
    };

    handleShowRoom = () => {
        this.setState({
            showRoom: true,
        });
    }

    handleHideRoom = () => {
        this.setState({
            showRoom: false,
        });
    };
}