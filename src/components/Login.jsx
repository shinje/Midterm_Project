import './Login.css';

export default class Login extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            email: '',
            password: '',
            isError: false,
            errorMessage: '',
            isSuccess: false,
            successMessage: ''
        };
    }

    render() {
        return (
            <div className='login mx-auto mt-3'>
                <Alert color='danger' isOpen={this.state.isError} >{this.state.errorMessage}</Alert>
                <Alert color='success' isOpen={this.state.isSuccess} >{this.state.successMessage}</Alert>
                <FormGroup>
                    <Input type='email' value={this.state.email} onChange={this.handleEmailChange} placeholder='電子信箱' bsSize='lg' />
                </FormGroup>
                <FormGroup>
                    <Input type='password' value={this.state.password} onChange={this.handlePasswordChange} placeholder='密碼' bsSize='lg' />
                </FormGroup>
                <FormGroup>
                    <Button color='primary' size='lg' block onClick={this.handleSignInClick}>登入</Button>
                    <Button color='danger' size='lg' block onClick={this.handleSignInGoogleClick}>Google 登入</Button>
                    <Button color='secondary' size='lg' block onClick={this.handleSignUpClick}>註冊</Button>
                </FormGroup>
            </div>
        );
    }

    componentWillUnmount() {
        this.props.onSignInSuccess();
    }

    handleEmailChange = e => {
        this.setState({
            email: e.target.value
        });
    };

    handlePasswordChange = e => {
        this.setState({
            password: e.target.value
        });
    };

    handleSignInClick = () => {
        firebase.auth().signInWithEmailAndPassword(this.state.email, this.state.password).then(user => {
            if (!user.emailVerified) {
                firebase.auth().signOut();
                throw {
                    code: 'auth/email-not-verified',
                    message: '您的電子信箱尚未驗證！'
                };
            }
        }).catch(error => {
            let errorMessage = '';
            switch (error.code) {
                case 'auth/invalid-email':
                    errorMessage = '信箱格式有誤！';
                    break;
                case 'auth/user-disabled':
                    errorMessage = '此用戶已被停權！';
                    break;
                case 'auth/user-not-found':
                    errorMessage = '查無此用戶！請先註冊！';
                    break;
                case 'auth/wrong-password':
                    errorMessage = '密碼不正確！';
                    break;
                default:
                    errorMessage = error.message;
            }
            this.setState({
                password: '',
                isSuccess: false,
                errorMessage: errorMessage,
                isError: true
            });
        });
    };

    handleSignInGoogleClick = () => {
        const provider = new firebase.auth.GoogleAuthProvider();
        firebase.auth().signInWithPopup(provider).catch(error => {
            let errorMessage = '';
            switch (error.code) {
                case 'auth/account-exists-with-different-credential':
                    errorMessage = '此帳戶已經存在！';
                    break;
                case 'auth/auth-domain-config-required':
                    errorMessage = '資料庫初始化失敗！';
                    break;
                case 'auth/cancelled-popup-request':
                    errorMessage = '登入遭到取消！';
                    break;
                case 'auth/operation-not-allowed':
                    errorMessage = '不支援的登入方式！';
                    break;
                case 'auth/operation-not-supported-in-this-environment':
                    errorMessage = '不支援此環境！';
                    break;
                case 'auth/popup-blocked':
                    errorMessage = '登入視窗遭瀏覽器阻擋！';
                    break;
                case 'auth/popup-closed-by-user':
                    errorMessage = '登入視窗遭到關閉！';
                    break;
                case 'auth/unauthorized-domain':
                    errorMessage = '此網域未經認證！';
                    break;
                default:
                    errorMessage = error.message;
            }
            this.setState({
                password: '',
                isSuccess: false,
                errorMessage: errorMessage,
                isError: true
            });
        });
    };

    handleSignUpClick = () => {
        firebase.auth().createUserWithEmailAndPassword(this.state.email, this.state.password).then(user => {
            this.setState({
                password: '',
                isError: false,
                successMessage: '註冊成功！請登入您的信箱進行驗證',
                isSuccess: true
            }, () => {
                user.sendEmailVerification().then(() => {
                    firebase.auth().signOut();
                }).catch(error => {
                    this.setState({
                        password: '',
                        isSuccess: false,
                        errorMessage: error.message,
                        isError: true
                    });
                });
            });
        }).catch(error => {
            let errorMessage = '';
            switch (error.code) {
                case 'auth/invalid-email':
                    errorMessage = '信箱格式有誤！';
                    break;
                case 'auth/email-already-in-use':
                    errorMessage = '此信箱已被使用！';
                    break;
                case 'auth/weak-password':
                    errorMessage = '密碼長度不足6字元！';
                    break;
                default:
                    errorMessage = error.message;
            }
            this.setState({
                password: '',
                isSuccess: false,
                errorMessage: errorMessage,
                isError: true
            });
        });
    }
}