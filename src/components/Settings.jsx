import './Settings.css';

export default class Settings extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            photoURL: '',
            email: '',
            displayName: '',
            displayNameValid: false,
            displayNameInvalid: false,
            displayNameMessage: '',
            phoneNumber: '',
            phoneNumberValid: false,
            phoneNumberInvalid: false,
            phoneNumberMessage: '',
            password: '',
            passwordValid: false,
            passwordInvalid: false,
            passwordMessage: ''
        };
    }

    render() {
        return (
            <div className={'settings' + (this.props.show ? ' show' : '')}>
                <div className='text-center mb-0'>
                    <Label for='photoURLInput'>
                        <div className='sticker' style={{ backgroundImage: `url(${this.state.photoURL})` }}></div>
                    </Label>
                    <Input type='file' id='photoURLInput' className='d-none' accept='.jpg, .jpeg, .png' onChange={this.handlePhotoUpload} />
                </div>
                <br />
                <InputGroup>
                    <InputGroupAddon addonType='prepend'>電子信箱</InputGroupAddon>
                    <Input type='email' id='emailInput' value={this.state.email} disabled />
                </InputGroup>
                <br />
                <InputGroup>
                    <InputGroupAddon addonType='prepend'>用戶名稱</InputGroupAddon>
                    <Input type='text' id='displayNameInput' value={this.state.displayName} onChange={this.handleDisplayNameChange} onFocus={this.handleFocus} valid={this.state.displayNameValid} invalid={this.state.displayNameInvalid} />
                    <InputGroupAddon addonType='append'>
                        <Button color='primary' onClick={this.handleDisplayNameUpdate}><i className="fas fa-check"></i></Button>
                    </InputGroupAddon>
                    <FormFeedback className='text-right' valid={this.state.displayNameValid}>{this.state.displayNameMessage}</FormFeedback>
                </InputGroup>
                <br />
                <InputGroup>
                    <InputGroupAddon addonType='prepend'>手機號碼</InputGroupAddon>
                    <Input type='text' pattern='[0-9]*' id='phoneNumberInput' value={this.state.phoneNumber} placeholder='尚未輸入手機號碼' onChange={this.handlePhoneNumberChange} onFocus={this.handleFocus} valid={this.state.phoneNumberValid} invalid={this.state.phoneNumberInvalid} />
                    <InputGroupAddon addonType='append'>
                        <Button color='primary' onClick={this.handlePhoneNumberUpdate}><i className="fas fa-check"></i></Button>
                    </InputGroupAddon>
                    <FormFeedback className='text-right' valid={this.state.phoneNumberValid}>{this.state.phoneNumberMessage}</FormFeedback>
                </InputGroup>
                <br />
                <InputGroup>
                    <InputGroupAddon addonType='prepend'>更改密碼</InputGroupAddon>
                    <Input type='password' id='passwordInput' value={this.state.password} placeholder='輸入新密碼' onChange={this.handlePasswordChange} onFocus={this.handleFocus} valid={this.state.passwordValid} invalid={this.state.passwordInvalid} />
                    <InputGroupAddon addonType='append'>
                        <Button color='primary' onClick={this.handlePasswordUpdate}><i className="fas fa-check"></i></Button>
                    </InputGroupAddon>
                    <FormFeedback className='text-right' valid={this.state.passwordValid}>{this.state.passwordMessage}</FormFeedback>
                </InputGroup>
                <Button color='danger' className='mt-4' block onClick={this.handleLogoutClick}>登出</Button>
            </div>
        );
    }

    componentDidMount() {
        firebase.firestore().doc(`users/${this.props.user.uid}`).onSnapshot(docSnapshot => {
            this.setState({
                ...docSnapshot.data(),
                password: ''
            });
        });
    }

    componentWillUnmount() {
        firebase.firestore().doc(`users/${this.props.user.uid}`).onSnapshot(() => { });
    }

    handlePhotoUpload = e => {
        if (e.target.files.length) {
            firebase.storage().ref(`users/${this.props.user.uid}`).put(e.target.files[0], {
                contentType: e.target.files[0].type
            }).then(snapshot => {
                firebase.firestore().doc(`users/${this.props.user.uid}`).update({
                    photoURL: snapshot.downloadURL
                }).then(() => {
                    this.state.photoURL = snapshot.downloadURL;
                });
            });
        }
    };

    handleDisplayNameChange = e => {
        this.setState({
            displayName: e.target.value
        });
    };

    handlePhoneNumberChange = e => {
        this.setState({
            phoneNumber: e.target.value
        });
    };

    handlePasswordChange = e => {
        this.setState({
            password: e.target.value
        });
    };

    handleDisplayNameUpdate = () => {
        if (/^[\u4e00-\u9fa5\w]+$/.test(this.state.displayName)) {
            firebase.firestore().doc(`users/${this.props.user.uid}`).update({
                displayName: this.state.displayName
            }).then(() => {
                this.setState({
                    displayNameValid: true,
                    displayNameInvalid: false,
                    displayNameMessage: '成功修改名稱'
                });
            });
        } else {
            this.setState({
                displayNameValid: false,
                displayNameInvalid: true,
                displayNameMessage: '名稱格式不符'
            });
        }
    };

    handlePhoneNumberUpdate = () => {
        if (/^09\d{8}$/.test(this.state.phoneNumber)) {
            firebase.firestore().doc(`users/${this.props.user.uid}`).update({
                phoneNumber: this.state.phoneNumber
            }).then(() => {
                this.setState({
                    phoneNumberValid: true,
                    phoneNumberInvalid: false,
                    phoneNumberMessage: '成功修改手機'
                });
            });
        } else {
            this.setState({
                phoneNumberValid: false,
                phoneNumberInvalid: true,
                phoneNumberMessage: '手機格式不符'
            });
        }
    };

    handlePasswordUpdate = () => {
        this.props.user.updatePassword(this.state.password).then(() => {
            this.setState({
                passwordValid: true,
                passwordInvalid: false,
                passwordMessage: '修改密碼成功'
            });
        }).catch(error => {
            let errorMessage = ''
            switch (error.code) {
                case 'auth/weak-password':
                    errorMessage = '密碼長度不足6字元';
                    break;
                case 'auth/requires-recent-login':
                    errorMessage = '請先重新登入';
                    break;
                default:
                    errorMessage = error.message;
            }
            this.setState({
                passwordValid: false,
                passwordInvalid: true,
                passwordMessage: errorMessage
            });
        });
    };

    handleFocus = () => {
        this.setState({
            displayNameValid: false,
            displayNameInvalid: false,
            phoneNumberValid: false,
            phoneNumberInvalid: false,
            passwordValid: false,
            passwordInvalid: false,
        });
    };

    handleLogoutClick = () => {
        firebase.auth().signOut();
    };
}