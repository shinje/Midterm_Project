import Friend from 'components/Friend.jsx';
import Add from 'components/Add.jsx';
import Chat from 'components/Chat.jsx';
import Settings from 'components/Settings.jsx';
import Footer from 'components/Footer.jsx';

import './Pages.css';

export default class Pages extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            page: 'Friend',
            friends: [],
            friendsList: [],
            addSearchInput: '',
            addSearchResults1: [],
            addSearchResults2: [],
            addSearchResults3: [],
            chatrooms: [],
            chatroomsList: [],
            showWhichRoom: '',
            cancelSubscribeFriends: null,
            cancelSubscribeChatrooms: null
        };
    }

    render() {
        return (
            <div className='pages'>
                <Friend show={this.state.page === 'Friend'} friendsList={this.state.friendsList}
                    onAgreeFriend={this.handleAgreeFriend} onRejectFriend={this.handleRejectFriend}
                    onChatWithFriend={this.handleChatWithFriend}
                    onAddFriend={this.handleAddFriend} onAgreeFriend={this.handleAgreeFriend}
                    onRejectFriend={this.handleRejectFriend} onChatWithFriend={this.handleChatWithFriend} />
                <Add show={this.state.page === 'Add'} friends={this.state.friends}
                    onSearchClick={this.handleAddSearchClick} onInputChange={this.handleAddInputChange}
                    input={this.state.addSearchInput} results1={this.state.addSearchResults1}
                    results2={this.state.addSearchResults2} results3={this.state.addSearchResults3}
                    onAddFriend={this.handleAddFriend} onAgreeFriend={this.handleAgreeFriend}
                    onRejectFriend={this.handleRejectFriend} onChatWithFriend={this.handleChatWithFriend} />
                <Chat show={this.state.page === 'Chat'} chatroomsList={this.state.chatroomsList}
                    showRoom={this.props.showRoom} showWhichRoom={this.state.showWhichRoom}
                    onShowRoomClick={this.handleShowRoom} onHideRoom={this.props.onHideRoom}
                    onSend={this.handleSend} user={this.props.user} />
                <Settings show={this.state.page === 'Settings'} user={this.props.user} />
                <Footer onFriendClick={this.handleFriendClick} onAddClick={this.handleAddClick}
                    onChatClick={this.handleChatClick} onSettingsClick={this.handleSettingsClick}
                    page={this.state.page} show={!this.props.showRoom} />
            </div>
        );
    }

    componentDidMount() {
        this.setState({
            cancelSubscribeFriends:
                firebase.firestore().collection(`users/${this.props.user.uid}/friends`).onSnapshot(snapshot => {
                    this.setState({
                        friends: snapshot.docs.slice()
                    }, () => {
                        this.handleAddSearchClick();
                        this.updateFriendsList();
                    });
                }),
            cancelSubscribeChatrooms:
                firebase.firestore().collection(`users/${this.props.user.uid}/chatrooms`).onSnapshot(snapshot => {
                    this.setState({
                        chatrooms: snapshot.docs.slice()
                    }, () => {
                        this.updateChatroomsList();
                    });
                })
        });
    }

    componentWillUnmount() {
        this.state.cancelSubscribeFriends();
        this.state.cancelSubscribeChatrooms();
    }

    handleFriendClick = () => this.setState({ page: 'Friend' });
    handleAddClick = () => this.setState({ page: 'Add' });
    handleChatClick = () => this.setState({ page: 'Chat' });
    handleSettingsClick = () => this.setState({ page: 'Settings' });

    updateFriendsList = () => {
        const friendsList = [];
        const promiseList = [];
        const determineType = friend => friend.get('accepted') ? 2 : !friend.get('isRequest') ? 1 : 0;
        for (let friend of this.state.friends) {
            if (determineType(friend) !== 1) {
                promiseList.push(
                    firebase.firestore().doc(`users/${friend.id}`).get().then(docSnapshot => {
                        friendsList.push({
                            id: docSnapshot.id,
                            type: determineType(friend),
                            ...docSnapshot.data()
                        });
                    })
                );
            }
        }
        Promise.all(promiseList).then(() =>
            this.setState({ friendsList: friendsList.slice() }));
    };


    handleAddInputChange = e => this.setState({ addSearchInput: e.target.value });

    handleAddSearchClick = () => {
        if (this.state.addSearchInput) {
            firebase.firestore().collection('users').where('displayName', '==', this.state.addSearchInput).get()
                .then(this.updateAddResults).then(arr => this.setState({ addSearchResults1: arr }));
            firebase.firestore().collection('users').where('email', '==', this.state.addSearchInput).get()
                .then(this.updateAddResults).then(arr => this.setState({ addSearchResults2: arr }));
            firebase.firestore().collection('users').where('phoneNumber', '==', this.state.addSearchInput).get()
                .then(this.updateAddResults).then(arr => this.setState({ addSearchResults3: arr }));
        } else {
            this.setState({
                addSearchResults1: [],
                addSearchResults2: [],
                addSearchResults3: []
            });
        }
    };

    updateAddResults = querySnapshot => {
        let arr = [];
        querySnapshot.forEach(docSnapshot => {
            let idx = this.state.friends.map(snapshot => snapshot.id).indexOf(docSnapshot.id);
            if (idx < 0) {
                arr.push({ id: docSnapshot.id, ...docSnapshot.data(), type: (docSnapshot.id === this.props.user.uid ? 'me' : 'strangers') });
            } else if (this.state.friends[idx].get('accepted')) {
                arr.push({ id: docSnapshot.id, ...docSnapshot.data(), type: 'friends' });
            } else if (this.state.friends[idx].get('isRequest')) {
                arr.push({ id: docSnapshot.id, ...docSnapshot.data(), type: 'request' });
            } else {
                arr.push({ id: docSnapshot.id, ...docSnapshot.data(), type: 'sent' });
            }
        });
        return arr;
    };

    handleAddFriend = user => {
        firebase.firestore().doc(`users/${this.props.user.uid}/friends/${user.id}`).set({
            accepted: false,
            isRequest: false,
            ref: firebase.firestore().doc(`users/${user.id}`)
        }).then(() => {
            firebase.firestore().doc(`users/${user.id}/friends/${this.props.user.uid}`).set({
                accepted: false,
                isRequest: true,
                ref: firebase.firestore().doc(`users/${this.props.user.uid}`)
            });
        });
    };

    handleAgreeFriend = user => {
        firebase.firestore().doc(`users/${this.props.user.uid}/friends/${user.id}`).update({ accepted: true });
        firebase.firestore().doc(`users/${user.id}/friends/${this.props.user.uid}`).update({ accepted: true });
    };

    handleRejectFriend = user => {
        firebase.firestore().doc(`users/${this.props.user.uid}/friends/${user.id}`).delete();
    };

    handleChatWithFriend = user => {
        this.handleChatClick();
        firebase.firestore().collection(`users/${this.props.user.uid}/chatrooms`).where('with', '==', user.id).get().then(chatroom => {
            if (chatroom.empty) {
                firebase.firestore().collection('chatrooms').where('member1', '==', user.id).where('member2', '==', this.props.user.uid).get().then(chatroom => {
                    if (chatroom.empty) {
                        firebase.firestore().collection('chatrooms').add({
                            member1: this.props.user.uid,
                            member2: user.id
                        }).then(chatroom => {
                            return firebase.firestore().doc(`users/${this.props.user.uid}/chatrooms/${chatroom.id}`).set({
                                ref: chatroom,
                                with: user.id,
                                timestamp: firebase.firestore.FieldValue.serverTimestamp(),
                                lastDialog: '尚無對話'
                            });
                        });
                    } else {
                        firebase.firestore().doc(`users/${this.props.user.uid}/chatrooms/${chatroom.docs[0].id}`).set({
                            ref: chatroom.docs[0].ref,
                            with: user.id,
                            timestamp: firebase.firestore.FieldValue.serverTimestamp(),
                            lastDialog: '尚無對話'
                        });
                    }
                });
            } else {
                this.handleShowRoom(this.state.chatroomsList.find(el => el.with === user.id));
            }
        });
    };

    updateChatroomsList = () => {
        const chatroomsList = [];
        const promiseList = [];
        for (let chatroom of this.state.chatrooms) {
            promiseList.push(
                firebase.firestore().doc(`users/${chatroom.get('with')}`).get().then(user => {
                    chatroomsList.push({
                        id: chatroom.id,
                        ...chatroom.data(),
                        ...user.data()
                    });
                })
            );
        }
        Promise.all(promiseList).then(() =>
            this.setState({ chatroomsList: chatroomsList.slice().sort((a, b) => b.timestamp - a.timestamp) }));
    };

    handleShowRoom = room => {
        this.setState({
            showWhichRoom: room
        });
        this.props.onShowRoom();
    };

    handleSend = (content, room) => {
        room.ref.collection('dialog').add({
            content: content,
            from: this.props.user.uid,
            timestamp: firebase.firestore.FieldValue.serverTimestamp()
        });
        firebase.firestore().doc(`users/${this.props.user.uid}/chatrooms/${room.id}`).update({
            lastDialog: content,
            timestamp: firebase.firestore.FieldValue.serverTimestamp()
        });
        firebase.firestore().doc(`users/${room.with}/chatrooms/${room.id}`).set({
            ref: room.ref,
            with: this.props.user.uid,
            lastDialog: content,
            timestamp: firebase.firestore.FieldValue.serverTimestamp()
        });
    };
}