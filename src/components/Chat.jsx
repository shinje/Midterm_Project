import Room from 'components/Room.jsx'

import './Chat.css';

export default class Chat extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className={`chat ${this.props.show ? ' show' : ''} ${this.props.showRoom ? ' container fixed-top mt-3' : ''}`}>
                {this.props.showRoom && <Room room={this.props.showWhichRoom} onHideRoom={this.props.onHideRoom} onSend={this.props.onSend} user={this.props.user} />}
                {!this.props.showRoom && this.props.chatroomsList.map(this.roomToDOM)}
            </div>
        );
    }

    roomToDOM = room => {
        return (
            <div key={room.id} className='room-item d-flex align-items-center mb-3 px-3 py-3' onClick={this.props.onShowRoomClick.bind(this, room)}>
                <div className='sticker' style={{ backgroundImage: `url(${room.photoURL})` }}></div>
                <div className='ml-3 ellipsis'>
                    {room.displayName}<br />
                    <small className='text-secondary ellipsis'>{room.lastDialog}</small>
                </div>
            </div>
        );
    };
}