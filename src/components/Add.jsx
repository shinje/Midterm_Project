import Search from 'components/Search.jsx';

import './Add.css';

export default class Add extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className={'add' + (this.props.show ? ' show' : '')}>
                <Search input={this.props.input} onInputChange={this.props.onInputChange}
                    onSearchClick={this.props.onSearchClick} placeholder='輸入信箱、名稱或手機' />
                <div className='result'>
                    {this.props.results1.map(this.resultToDOM)}
                    {this.props.results2.map(this.resultToDOM)}
                    {this.props.results3.map(this.resultToDOM)}
                </div>
            </div>
        );
    }

    resultToDOM = result => {
        const typeToBtn = {
            strangers:
                (<Button color='primary' className='mt-2' onClick={this.handleAddFriend.bind(this, result)}>
                    <i className='fas fa-user-plus'></i>
                </Button>),
            friends:
                (<Button color='info' className='mt-2' onClick={this.handleChatWithFriend.bind(this, result)}>
                    <i className='fas fa-comments'></i>
                </Button>),
            request:
                (<div>
                    <Button color='success' className='mx-1 mt-2' onClick={this.handleAgreeFriend.bind(this, result)}>
                        <i className='fas fa-check'></i>
                    </Button>
                    <Button color='danger' className='mx-1 mt-2' onClick={this.handleRejectFriend.bind(this, result)}>
                        <i className='fas fa-times' style={{ width: '14px' }}></i>
                    </Button>
                </div>),
            sent:
                (<Button color='primary' className='mt-2' disabled>
                    <i className='fas fa-user-plus'></i>
                </Button >)
        };
        return (
            <div key={result.id} className='d-flex flex-column justify-content-center align-items-center mt-4 py-4 rounded' style={{ background: '#ffffff07' }}>
                <div className='sticker my-3' style={{ backgroundImage: `url(${result.photoURL})` }}></div>
                {result.displayName}
                <small className='text-secondary'>{result.type === 'me' ? '能找到自己嗎？' : result.email}</small>
                {typeToBtn[result.type]}
            </div>
        );
    };

    handleAddFriend = result => this.props.onAddFriend(result);
    handleAgreeFriend = result => this.props.onAgreeFriend(result);
    handleRejectFriend = result => this.props.onRejectFriend(result);
    handleChatWithFriend = result => this.props.onChatWithFriend(result);
}